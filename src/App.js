import React, { Component } from 'react';
//import Human from './components/Human';
//import Vehicle from './components/Vehicle';
//import Animal from './components/Animal';

import ValidationComponent from './components/ValidationComponent';
import CharComponent from './components/CharComponent';

import './App.css';

class App extends Component {

  state = {
    inputText:"",
    inputTextLength:0
  }

  inputTextChangeHandler = (event) => {
    this.setState({
      inputText:event.target.value,
      inputTextLength: event.target.value.length
    });
  }

  deleteHandler = (index) => {
    let text = [...this.state.inputText.split('')];
    text.splice(index,1);
    this.setState({
      inputText:text.join(''),
      inputTextLength: text.length
    });
  }

  render() {

    let inputTextArray = [...this.state.inputText.split('')];
    console.log(inputTextArray)
    let charComponentArray = inputTextArray.map((letter,index) => {
      return (
      <CharComponent 
        letter={letter}
        key={index}
        delete={this.deleteHandler.bind(this,index)} />
      );
    });

    return (
      <div className="App">
        <h2>App Component updated</h2>
        <input type="text" onChange={this.inputTextChangeHandler} value={this.state.inputText} />
        <p>Length: {this.state.inputTextLength}</p>
        <ValidationComponent length={this.state.inputTextLength} />

        {charComponentArray}
        
      </div>
    );
  }
}

export default App;
