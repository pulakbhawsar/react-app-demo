import React from 'react';

const WildAnimal = (props) => {
    return (
        <p>Wild animal name:{props.name}</p>
    );
}

export default WildAnimal;