import React from 'react';

const CharComponent = (props) => {
    const inlineBoxStyle = {
        display: "inline-block",
        padding: "16px",
        textAlign: "center",
        margin: "16px",
        border: "1px solid black"
    }
    return (
        <div style={inlineBoxStyle} onClick={props.delete}>
            {props.letter}
        </div>
    );
}


export default CharComponent;