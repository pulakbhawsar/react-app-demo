import React, {Component} from 'react';

import Man from './Man';

class Human extends Component {
    state = {
        persons: [
            {name:"Anil", age:30},
            {name:"Suresh", age:25},
            {name:"Ramesh", age:28},
        ]
    };

    addSalutationHandler = () => {
        this.setState({
            persons: [
                {name:"Mr. Anil", age:30},
                {name:"Mr. Suresh", age:25},
                {name:"Mr. Ramesh", age:28},
            ]
        });
    }

    render(){
        return(
            <div>
                <p>Hello Human type: {this.props.humanType}</p>
                <Man name={this.state.persons[0].name} />
                <Man name={this.state.persons[1].name} />
                <Man name={this.state.persons[2].name} />
                <button onClick={this.addSalutationHandler}>Add Salutation</button>
            </div>
        );
    }
}

export default Human

