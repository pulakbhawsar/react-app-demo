import React from 'react';


const Vehicle = (props) => {
    return (
        <p>This is vehicle with color: {props.color}</p>
    );
}

export default Vehicle;