import React, {useState } from 'react';
import WildAnimal from './WildAnimal';

const Animal = (props) => {
    const [animalState, setAnimalState] = useState({
        animals:[
            {"name" : "Tiger", weight:300},
            {"name" : "Elephant", weight:1000},
            {"name" : "Horse", weight:300}
        ]
    });

    const pluralizeHandler = () => {
        setAnimalState({ animals:[
            {"name" : "Tigers", weight:300},
            {"name" : "Elephants", weight:1000},
            {"name" : "Horses", weight:300}
        ]});
    }


    return (
        <div>
            <p>Type of Animals: {props.animalType}</p>
            <WildAnimal name={animalState.animals[0].name} />
            <WildAnimal name={animalState.animals[1].name} />
            <WildAnimal name={animalState.animals[2].name} />
            <button onClick={pluralizeHandler} >Pluralize</button>
        </div>
        
    );
}

export default Animal;