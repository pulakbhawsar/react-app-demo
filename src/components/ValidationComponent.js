import React from 'react';

const ValidationComponent = (props) => {
    const lengthLimit = 5;

    let validationText = ((props.length < lengthLimit)?"Text too short":"Text long enough");
    return <p>{validationText}</p>
}

export default ValidationComponent;